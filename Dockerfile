FROM node:6-alpine

ENV LIB_ROOT /enrollment
RUN mkdir -p $LIB_ROOT

WORKDIR $LIB_ROOT

COPY package.json package.json

RUN npm install

COPY . .

RUN chown -R nobody:nogroup $LIB_ROOT

USER nobody
CMD ["docker/test.sh"]
