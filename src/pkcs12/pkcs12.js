import createP12 from './forge-ext'
import { privateKeyToPEM } from './private-key'

export default {
  create({ certificateChain, key, password }) {
    return privateKeyToPEM(key)
    .then(pemKey => {
      return createP12(
        pemKey,
        certificateChain,
        password,
        { algorithm: '3des' }
      );
    });
  }
}
