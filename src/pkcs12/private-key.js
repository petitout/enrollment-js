import webcrypto from 'lib/webcrypto'
import { formatBuffer, Types } from 'lib/pem'

const crypto = webcrypto.create();

const privateKeyToPEM = keyPair => {
  return crypto.exportKey(
    'pkcs8',
    keyPair.privateKey
  ).then(buffer => {
    return formatBuffer(buffer, Types.PRIVATE_KEY);
  });
}

export { privateKeyToPEM }
