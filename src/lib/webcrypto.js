import * as pkijs from 'pkijs'

export default {
  create() {
    return pkijs.getCrypto();
  }
}
