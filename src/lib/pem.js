import * as asn1js from 'asn1js'
import {
  arrayBufferToString,
  stringToArrayBuffer,
  fromBase64,
  toBase64
} from 'pvutils'

const Types = {
  CERTIFICATE: 'CERTIFICATE',
  CERTIFICATE_REQUEST: 'CERTIFICATE REQUEST',
  PRIVATE_KEY: 'PRIVATE KEY'
};

function addLineBreaks(base64) {
  const length = base64.length;
  let result = '';

  for(let i = 0, count = 0; i < length; i++, count++) {
    if(count > 63) {
      result = result + '\n';
      count = 0;
    }
    result = `${result}${base64[i]}`;
  }

  return result;
}

function addHeaderFooter(base64, type) {
  return `-----BEGIN ${type}-----\n${base64}\n-----END ${type}-----\n`;
}

function pemRegexpFor(type) {
  return new RegExp(`(\\s*-+(BEGIN|END)\\s+${type}-+)|\\n`, 'g');
}

export { Types };

export function formatBase64(base64, type) {
  return addHeaderFooter(
    addLineBreaks(base64),
    type
  );
}

export function formatBuffer(buffer, type) {
  return formatBase64(
    bufferToBase64(buffer),
    type
  );
}

export function stripHeaderFooter(base64, type) {
  return base64.replace(pemRegexpFor(type), '');
}

export function parseASN1(pem, type) {
  if (!type) throw 'Type must be provided for parseASN1';

  return asn1js.fromBER(
    stringToArrayBuffer(fromBase64(stripHeaderFooter(pem, type)))
  );
}

export function bufferToBase64(buffer) {
  return toBase64(arrayBufferToString(buffer));
}

