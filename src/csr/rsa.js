import webcrypto from 'lib/webcrypto'

export default (options) => {
  return webcrypto.create().generateKey(
    {
      name: 'RSASSA-PKCS1-v1_5',
      modulusLength: options.keyLength || 2048,
      publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
      hash: { name: options.hash || 'SHA-256' }
    },
    true,
    ['sign', 'verify']
  );
}
