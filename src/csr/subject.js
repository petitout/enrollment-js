import * as pkijs from 'pkijs'
import * as asn1js from 'asn1js'

const DN_TO_OID = {
  C: '2.5.4.6',
  OU: '2.5.4.10',
  O: '2.5.4.11',
  CN: '2.5.4.3',
  L: '2.5.4.7',
  S: '2.5.4.8',
  T: '2.5.4.12',
  GN: '2.5.4.42',
  I: '2.5.4.43',
  SN: '2.5.4.4',
  EMAIL: '1.2.840.113549.1.9.1'
};


function parseSubject(subject) {
  const DN_REGEX = /\/([A-Z]+)=([^\/]+)/g;
  let match = DN_REGEX.exec(subject);
  const results = [];

  while (match) {
    results.push([match[1], match[2]]);
    match = DN_REGEX.exec(subject);
  }

  return results;
}

function processOptions(subject) {
  return parseSubject(subject).map(([rdn, value]) => {
    return [DN_TO_OID[rdn], value];
  });
}

function createSubject(options) {
  return processOptions(options.subject).map(([oid, value]) => {
    return new pkijs.AttributeTypeAndValue({
      type: oid,
      value: new asn1js.PrintableString({ value: value })
    });
  });
}

export function create(pkcs10, options) {
  createSubject(options).forEach(attrTypeAndValue => {
    pkcs10.subject.typesAndValues.push(attrTypeAndValue);
  });
  return Promise.resolve(pkcs10);
}
