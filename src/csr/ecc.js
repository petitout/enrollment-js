import webcrypto from 'lib/webcrypto'

export default (options) => {
  return webcrypto.create().generateKey(
    {
      name: 'ECDSA',
      namedCurve: options.curve || 'P-256',
    },
    true,
    ['sign', 'verify']
  );
}
