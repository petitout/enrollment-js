import * as pkijs from 'pkijs'
import { formatBuffer, Types } from 'lib/pem'
import { create as createSubject } from './subject'
import ecc from './ecc'
import rsa from './rsa'

const createWithKeyPair = (keyPair, options) => {
  const pkcs10 = new pkijs.CertificationRequest();
  pkcs10.version = 0;

  return createSubject(pkcs10, options).then(() => {
    return pkcs10.subjectPublicKeyInfo.importKey(keyPair.publicKey);
  }).then(() => {
    return pkcs10.sign(keyPair.privateKey, 'SHA-256');
  }).then(() => {
    return {
      csr: formatBuffer(
        pkcs10.toSchema().toBER(),
        Types.CERTIFICATE_REQUEST
      ),
      key: keyPair
    };
  });
}

export default {
  create(options) {
    if (!options.type) {
      throw 'No type chosen'
    }

    let keyPairFn

    switch (options.type) {
    case 'ECC':
      keyPairFn = ecc;
      break;
    case 'RSA':
      keyPairFn = rsa;
      break;
    }

    return keyPairFn(options).then(keyPair => {
      return createWithKeyPair(keyPair, options);
    });
  }
}
