const csr = require('./csr/csr.js')


exports.generateCSR = (options) => {
  return csr.create(options);
}
