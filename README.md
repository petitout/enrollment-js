# enrollment-js

A JavaScript library to support client-side handling of PKCS#10 requests and
generating PKCS#12 files from a private key and a certificate chain.

## Building the library

Make sure you have a recent installation of [node.js](nodejs.org) with NPM
installed.

To install the dependencies, run

```
$ npm install
```

You may build the library with

```
$ npm run build
```

which will create the final minified JS file in the `lib` directory.

### Linting

Each build will automatically lint the sources via ESLint, if you
wish to just lint the files without building the library, you may
use

```
$ npm run lint
```

## License

enrollment-js is released under the [MIT License](http://www.opensource.org/licenses/MIT).
